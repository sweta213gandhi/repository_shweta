package ParallelTestingPackage;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.annotations.BeforeMethod;

public class SetUp00
{
	WebDriver driver;

	@BeforeMethod
	public void OpenChrome() throws InterruptedException
	{
		System.setProperty("webdriver.chrome.driver", "C:\\Users\\dell\\Desktop\\Selenium Webdriver\\chromedriver.exe");
		
		driver = new ChromeDriver();
		
		driver.manage().window().maximize();
		
		driver.get("https://askzuma.com/");

		Thread.sleep(5000);
	}

	@BeforeMethod
	public void OpenFireFox() throws InterruptedException
	{
		System.setProperty("webdriver.gecko.driver", "C:\\Users\\dell\\Desktop\\Selenium Webdriver\\geckodriver.exe");
		
		driver = new FirefoxDriver();
		
		driver.manage().window().maximize();
		
		driver.get("https://askzuma.com/");

		Thread.sleep(5000);
	}
}
