package ParallelTestingPackage;

import org.openqa.selenium.By;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;

public class Login00 extends SetUp00
{
	@AfterMethod
	public void LoginAskZuma() throws InterruptedException
	{
		//	BLANK VALUE :

		driver.findElement(By.xpath("/html/body/div[16]/header/div/nav/ul[2]/li[1]/a")).click();
		
		driver.findElement(By.xpath("/html/body/div[16]/header/div/nav/div/div/form/footer/button")).click();
			
		Thread.sleep(2000);
	
		String actual1 = driver.findElement(By.xpath("/html/body/div[16]/header/div/nav/div/div/form/div[1]/div[1]/span")).getText();
	
		String expected1 = "Required";
	
		Assert.assertEquals(actual1, expected1);
		
		String actual2 = driver.findElement(By.xpath("/html/body/div[16]/header/div/nav/div/div/form/div[1]/div[2]/span")).getText();
		
		String expected2 = "Required";

		Assert.assertEquals(actual2, expected2);
			
		Thread.sleep(2000);
		
		
		//	INVALID EMAIL :
		
		driver.navigate().refresh();
		
		Thread.sleep(2000);
			
		driver.findElement(By.xpath("/html/body/div[16]/header/div/nav/ul[2]/li[1]/a")).click();
			
		driver.findElement(By.id("Email")).sendKeys("qwe@gmail.com");
		
		driver.findElement(By.id("Password")).sendKeys("Xyz@0000");
		
		driver.findElement(By.xpath("/html/body/div[16]/header/div/nav/div/div/form/footer/button")).click();
			
		Thread.sleep(2000);
			
		String actual5 = driver.findElement(By.xpath("/html/body/div[12]/div/div/div/div/div")).getText();
	
		String expected5 = "Invalid email or password.";
	
		Assert.assertEquals(actual5, expected5);
		
		driver.findElement(By.xpath("/html/body/div[12]/div/div/div/div/footer/button")).click();
			
		Thread.sleep(2000);
		
		
		//	INVALID PASSWORD :
		
		driver.navigate().refresh();
		
		Thread.sleep(2000);
	
		driver.findElement(By.xpath("/html/body/div[16]/header/div/nav/ul[2]/li[1]/a")).click();
			
		driver.findElement(By.id("Email")).sendKeys("sweta.213.gandhi@gmail.com");
		
		driver.findElement(By.id("Password")).sendKeys("qwe");
		
		driver.findElement(By.xpath("/html/body/div[16]/header/div/nav/div/div/form/footer/button")).click();
			
		Thread.sleep(2000);
		
		String actual6 = driver.findElement(By.xpath("/html/body/div[12]/div/div/div/div/div")).getText();
	
		String expected6 = "Invalid email or password.";
	
		Assert.assertEquals(actual6, expected6);
		
		driver.findElement(By.xpath("/html/body/div[12]/div/div/div/div/footer/button")).click();
			
		Thread.sleep(2000);
		
		
		//	VALID DETAILS :
		
		driver.navigate().refresh();
		
		Thread.sleep(2000);
		
		driver.findElement(By.xpath("/html/body/div[16]/header/div/nav/ul[2]/li[1]/a")).click();
		
		driver.findElement(By.xpath("//*[@id='Email']")).sendKeys("ankur1.manish@gmail.com");
		
		driver.findElement(By.xpath("//*[@id='Password']")).sendKeys("12345678");
		
		driver.findElement(By.xpath("//*[@class='button yellow btnSignin']")).click();
		
		Thread.sleep(3000);
	}
}
