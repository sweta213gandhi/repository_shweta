package ParallelTestingPackage;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.Assert;
import org.testng.annotations.Test;

public class Login
{
	WebDriver driver;
	
	@Test (priority = 0)
		public void ChromeLogin() throws InterruptedException
		{
			System.setProperty("webdriver.chrome.driver", "C:\\Users\\dell\\Desktop\\Selenium Webdriver\\chromedriver.exe");
			
			driver = new ChromeDriver();
			
			driver.manage().window().maximize();
	
			driver.get("https://askzuma.com/");
	
			Thread.sleep(5000);
			
			
			//	BLANK VALUE :
			
			driver.findElement(By.xpath("/html/body/div[16]/header/div/nav/ul[2]/li[1]/a")).click();
			
			driver.findElement(By.xpath("/html/body/div[16]/header/div/nav/div/div/form/footer/button")).click();
				
			Thread.sleep(2000);
		
			String actual1 = driver.findElement(By.xpath("/html/body/div[16]/header/div/nav/div/div/form/div[1]/div[1]/span")).getText();
		
			String expected1 = "Required";
		
			Assert.assertEquals(actual1, expected1);
			
			String actual2 = driver.findElement(By.xpath("/html/body/div[16]/header/div/nav/div/div/form/div[1]/div[2]/span")).getText();
			
			String expected2 = "Required";

			Assert.assertEquals(actual2, expected2);
				
			Thread.sleep(2000);
			
			
			//	INVALID EMAIL :
			
			driver.navigate().refresh();
			
			Thread.sleep(2000);
				
			driver.findElement(By.xpath("/html/body/div[16]/header/div/nav/ul[2]/li[1]/a")).click();
				
			driver.findElement(By.id("Email")).sendKeys("qwe@gmail.com");
			
			driver.findElement(By.id("Password")).sendKeys("Xyz@0000");
			
			driver.findElement(By.xpath("/html/body/div[16]/header/div/nav/div/div/form/footer/button")).click();
				
			Thread.sleep(2000);
				
			String actual5 = driver.findElement(By.xpath("/html/body/div[12]/div/div/div/div/div")).getText();
		
			String expected5 = "Invalid email or password.";
		
			Assert.assertEquals(actual5, expected5);
			
			driver.findElement(By.xpath("/html/body/div[12]/div/div/div/div/footer/button")).click();
				
			Thread.sleep(2000);
			
			
			//	INVALID PASSWORD :
			
			driver.navigate().refresh();
			
			Thread.sleep(2000);
		
			driver.findElement(By.xpath("/html/body/div[16]/header/div/nav/ul[2]/li[1]/a")).click();
				
			driver.findElement(By.id("Email")).sendKeys("sweta.213.gandhi@gmail.com");
			
			driver.findElement(By.id("Password")).sendKeys("qwe");
			
			driver.findElement(By.xpath("/html/body/div[16]/header/div/nav/div/div/form/footer/button")).click();
				
			Thread.sleep(2000);
			
			String actual6 = driver.findElement(By.xpath("/html/body/div[12]/div/div/div/div/div")).getText();
		
			String expected6 = "Invalid email or password.";
		
			Assert.assertEquals(actual6, expected6);
			
			driver.findElement(By.xpath("/html/body/div[12]/div/div/div/div/footer/button")).click();
				
			Thread.sleep(2000);
			
			
			//	VALID DETAILS :
			
			driver.navigate().refresh();
			
			Thread.sleep(2000);
			
			driver.findElement(By.xpath("/html/body/div[16]/header/div/nav/ul[2]/li[1]/a")).click();
			
			driver.findElement(By.xpath("//*[@id='Email']")).sendKeys("ankur1.manish@gmail.com");
			
			driver.findElement(By.xpath("//*[@id='Password']")).sendKeys("12345678");
			
			driver.findElement(By.xpath("//*[@class='button yellow btnSignin']")).click();
			
			Thread.sleep(3000);
		}


	@Test (priority = 1)
	public void FireFoxLogin() throws InterruptedException
	{
		System.setProperty("webdriver.gecko.driver", "C:\\Users\\dell\\Desktop\\Selenium Webdriver\\geckodriver.exe");
		
		driver = new FirefoxDriver();
		
		driver.manage().window().maximize();

		driver.get("https://askzuma.com/");

		Thread.sleep(5000);
		
		
		//	BLANK VALUE :
		
		driver.findElement(By.xpath("/html/body/div[16]/header/div/nav/ul[2]/li[1]/a")).click();
		
		driver.findElement(By.xpath("/html/body/div[16]/header/div/nav/div/div/form/footer/button")).click();
			
		Thread.sleep(2000);
	
		String actual1 = driver.findElement(By.xpath("/html/body/div[16]/header/div/nav/div/div/form/div[1]/div[1]/span")).getText();
	
		String expected1 = "Required";
	
		Assert.assertEquals(actual1, expected1);
		
		String actual2 = driver.findElement(By.xpath("/html/body/div[16]/header/div/nav/div/div/form/div[1]/div[2]/span")).getText();
		
		String expected2 = "Required";

		Assert.assertEquals(actual2, expected2);
			
		Thread.sleep(2000);
		
		
		//	INVALID EMAIL :
		
		driver.navigate().refresh();
		
		Thread.sleep(2000);
			
		driver.findElement(By.xpath("/html/body/div[16]/header/div/nav/ul[2]/li[1]/a")).click();
			
		driver.findElement(By.id("Email")).sendKeys("qwe@gmail.com");
		
		driver.findElement(By.id("Password")).sendKeys("Xyz@0000");
		
		driver.findElement(By.xpath("/html/body/div[16]/header/div/nav/div/div/form/footer/button")).click();
			
		Thread.sleep(2000);
			
		String actual5 = driver.findElement(By.xpath("/html/body/div[12]/div/div/div/div/div")).getText();
	
		String expected5 = "Invalid email or password.";
	
		Assert.assertEquals(actual5, expected5);
		
		driver.findElement(By.xpath("/html/body/div[12]/div/div/div/div/footer/button")).click();
			
		Thread.sleep(2000);
		
		
		//	INVALID PASSWORD :
		
		driver.navigate().refresh();
		
		Thread.sleep(2000);
	
		driver.findElement(By.xpath("/html/body/div[16]/header/div/nav/ul[2]/li[1]/a")).click();
			
		driver.findElement(By.id("Email")).sendKeys("sweta.213.gandhi@gmail.com");
		
		driver.findElement(By.id("Password")).sendKeys("qwe");
		
		driver.findElement(By.xpath("/html/body/div[16]/header/div/nav/div/div/form/footer/button")).click();
			
		Thread.sleep(2000);
		
		String actual6 = driver.findElement(By.xpath("/html/body/div[12]/div/div/div/div/div")).getText();
	
		String expected6 = "Invalid email or password.";
	
		Assert.assertEquals(actual6, expected6);
		
		driver.findElement(By.xpath("/html/body/div[12]/div/div/div/div/footer/button")).click();
			
		Thread.sleep(5000);
		
		
		//	VALID DETAILS :
		
		driver.navigate().refresh();
		
		Thread.sleep(2000);
		
		driver.findElement(By.xpath("/html/body/div[16]/header/div/nav/ul[2]/li[1]/a")).click();
		
		driver.findElement(By.xpath("//*[@id='Email']")).sendKeys("ankur1.manish@gmail.com");
		
		driver.findElement(By.xpath("//*[@id='Password']")).sendKeys("12345678");
		
		driver.findElement(By.xpath("//*[@class='button yellow btnSignin']")).click();
		
		Thread.sleep(3000);
	}
}
